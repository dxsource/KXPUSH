//
//  main.m
//  KXPush
//
//  Created by douglas525264 on 03/01/2019.
//  Copyright (c) 2019 douglas525264. All rights reserved.
//

@import UIKit;
#import "KXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KXAppDelegate class]));
    }
}
