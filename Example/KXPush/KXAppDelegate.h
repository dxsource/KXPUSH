//
//  KXAppDelegate.h
//  KXPush
//
//  Created by douglas525264 on 03/01/2019.
//  Copyright (c) 2019 douglas525264. All rights reserved.
//

@import UIKit;

@interface KXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
