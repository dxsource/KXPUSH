# KXPush

[![CI Status](https://img.shields.io/travis/douglas525264/KXPush.svg?style=flat)](https://travis-ci.org/douglas525264/KXPush)
[![Version](https://img.shields.io/cocoapods/v/KXPush.svg?style=flat)](https://cocoapods.org/pods/KXPush)
[![License](https://img.shields.io/cocoapods/l/KXPush.svg?style=flat)](https://cocoapods.org/pods/KXPush)
[![Platform](https://img.shields.io/cocoapods/p/KXPush.svg?style=flat)](https://cocoapods.org/pods/KXPush)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KXPush is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KXPush'
```

## Author

douglas525264, xin.dong@dewmobile.net

## License

KXPush is available under the MIT license. See the LICENSE file for more info.
